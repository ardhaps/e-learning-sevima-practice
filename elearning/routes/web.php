<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('login.login');
});

/* Route: AUTENTIKASI */
Route::get('/login', 'Auth\LoginController@showLoginForm')->name('showLogin');
Route::post('/login', 'Auth\LoginController@login')->name('login');
Route::get('/register', 'Auth\RegisterController@showRegisterForm')->name('showRegister');
Route::post('/register', 'Auth\RegisterController@register')->name('register');
Route::get('/logout', 'Auth\LoginController@logout')->name('logout');

Route::group(['middleware' => ['auth', 'dosen']], function() {

    /* Route: POSTING */
    Route::get('/posting', 'Posting\PostingIndexCtl@index')->name('indexPosting');
    Route::post('/posting/tambahposting', 'Posting\PostingCrudCtl@tambahPosting')->name('tambahPosting');
    Route::post('/posting/hapusposting/{id}', 'Posting\PostingCrudCtl@hapusPosting')->name('hapusPosting');

    /* Route: KONTEN POSTING */
    Route::get('/kontendosen/{id}', 'Konten\KontenDosenIndexCtl@index')->name('indexKontenDosen');
    Route::post('/kontendosen/tambahkomentar/{id}', 'Konten\KontenDosenCrudCtl@tambahKomentar')->name('tambahKomentarKontenDosen');
    Route::post('/kontendosen/hapuskomentar/{id}', 'Konten\KontenDosenCrudCtl@hapusKomentar')->name('hapusKomentarKontenDosen');

    Route::get('/downloaddosen/{file}', 'Download\DownloadCtl@downloadFile')->name('downloadFileDosen');


});

Route::group(['middleware' => ['auth', 'mahasiswa']], function() {
    
    /* Route: DAFTAR MATERI */
    Route::get('/daftarmateri', 'DaftarMateri\DaftarMateriIndexCtl@index')->name('indexDaftarMateri');
    
    /* Route: KONTEN MATERI */
    Route::get('/kontenmahasiswa/{id}', 'Konten\KontenMahasiswaIndexCtl@index')->name('indexKontenMahasiswa');
    Route::post('/kontenmahasiswa/{id}', 'Konten\KontenMahasiswaCrudCtl@tambahKomentar')->name('tambahKomentarKontenMahasiswa');

    Route::get('/downloadmahasiswa/{file}', 'Download\DownloadCtl@downloadFile')->name('downloadFileMahasiswa');


});
<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

/**
 * @property int $id_kontenmateri
 * @property int $id_materi
 * @property string $kontenmateri
 * @property Materi $materi
 */
class KontenMateri extends Model
{
    /**
     * The table associated with the model.
     * 
     * @var string
     */
    protected $table = 'kontenmateri';

    /**
     * @var array
     */
    protected $fillable = ['kontenmateri', 'file'];

    /**
     * Indicates if the model should be timestamped.
     * 
     * @var bool
     */
    public $timestamps = false;

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function materi()
    {
        return $this->belongsTo('App\Models\Materi', 'id_materi', 'id_materi');
    }
}

<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

/**
 * @property int $id_komentar
 * @property int $id_user
 * @property int $id_materi
 * @property string $komentar
 * @property Materi $materi
 * @property User $user
 */
class Komentar extends Model
{
    /**
     * The table associated with the model.
     * 
     * @var string
     */
    protected $table = 'komentar';

    /**
     * @var array
     */
    protected $fillable = ['komentar'];

    /**
     * Indicates if the model should be timestamped.
     * 
     * @var bool
     */
    public $timestamps = false;

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function materi()
    {
        return $this->belongsTo('App\Models\Materi', 'id_materi', 'id_materi');
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function user()
    {
        return $this->belongsTo('App\Models\User', 'id_user', 'id_user');
    }
}

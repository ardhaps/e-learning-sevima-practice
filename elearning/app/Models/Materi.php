<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

/**
 * @property int $id_materi
 * @property int $id_user
 * @property string $nama_materi
 * @property string $deskripsi_materi
 * @property int $durasi_materi
 * @property User $user
 */
class Materi extends Model
{
    /**
     * The table associated with the model.
     * 
     * @var string
     */
    protected $table = 'materi';

    /**
     * @var array
     */
    protected $fillable = ['id_user', 'nama_materi', 'deskripsi_materi', 'durasi_materi'];

    /**
     * Indicates if the model should be timestamped.
     * 
     * @var bool
     */
    public $timestamps = false;

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function user()
    {
        return $this->belongsTo('App\Models\User', 'id_user', 'id_user');
    }
}

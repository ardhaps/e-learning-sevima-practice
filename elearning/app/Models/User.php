<?php

namespace App\Models;

use Illuminate\Notifications\Notifiable;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Illuminate\Database\Eloquent\Model;

/**
 * @property int $id_user
 * @property int $id_hakakses
 * @property string $username
 * @property string $password
 * @property string $remember_token
 * @property HakAksess $hakAkses
 */
class User extends Authenticatable
{
    use Notifiable;
    
    /**
     * The table associated with the model.
     * 
     * @var string
     */
    protected $table = 'user';
    protected $primaryKey = 'id_user';


    /**
     * @var array
     */
    protected $fillable = ['nama', 'id_hakakses', 'username', 'password'];

    protected $hidden = [
        'password', 'remember_token',
    ];

    /**
     * Indicates if the model should be timestamped.
     * 
     * @var bool
     */
    public $timestamps = false;

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function hakAkse()
    {
        return $this->belongsTo('App\Models\HakAkses', 'id_hakakses', 'id_hakakses');
    }
}

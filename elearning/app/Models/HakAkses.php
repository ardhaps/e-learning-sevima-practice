<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

/**
 * @property int $id_hakakses
 * @property string $hakakses
 */
class HakAkses extends Model
{
    /**
     * The table associated with the model.
     * 
     * @var string
     */
    protected $table = 'hakakses';

    /**
     * The primary key for the model.
     * 
     * @var string
     */
    protected $primaryKey = 'id_hakakses';

    /**
     * @var array
     */
    protected $fillable = ['hakakses'];

    /**
     * Indicates if the model should be timestamped.
     * 
     * @var bool
     */
    public $timestamps = false;

}

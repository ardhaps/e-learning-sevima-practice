<?php

namespace App\Http\Controllers\DaftarMateri;

use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\DB;

class DaftarMateriIndexCtl extends Controller
{
	public function index()
    {

        $materis    = DB::table('Materi')
                    ->leftJoin('KontenMateri', 'Materi.id_materi', '=', 'KontenMateri.id_materi')
                    ->leftJoin('User', 'Materi.id_user', '=', 'User.id_user')
                    ->orderBy('Materi.id_materi', 'DESC')    
                    ->get();
        

		return view('daftarmateri.daftarmateri', compact('materis'));
    }


}

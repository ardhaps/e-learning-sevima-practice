<?php
    namespace App\Http\Controllers\Api;

    use App\Http\Controllers\Controller;
    use Illuminate\Http\Request;
    use App\Models\User;

    class ApiAuth extends Controller
    {
        public function login(Request $req)
        {
            $hasher         = app()->make('hash');
            $username       = $req->input('username');
            $password       = $req->input('password');
            
            $loginAkun      = User::where('username', $username)
                                ->first();


            if (!$loginAkun) {
                return response()->json([
                    'success'       => false,
                    'message'       => 'Ada kesalahan pada sistem'
                ], 500);
            }
            else {
                if ($hasher->check($password, $loginAkun->password)) {
                    
                    $rememberToken  = sha1(time());
                    $newToken       = User::where('id_user', $loginAkun->id_user)
                                        ->update(['remember_token' => $rememberToken]);
                    
                    if ($newToken) {                        
                        return response()->json([
                            'success'       => true,
                            'message'       => 'Autentikasi berhasil',
                            'loginData'     => $loginAkun
                        ], 200);
                    }
                } 
                else {
                    return response()->json([
                        'success'       => false,
                        'message'       => 'Autentikasi salah'
                    ], 400);
                }
            }
        }

        public function register(Request $req)
        {
            $username               = $req->input('username');
            $hasher                 = app()->make('hash');
            $password               = $hasher->make($req->input('password'));
            $nama                   = $req->input('nama');
            
            $id_hakakses            = $req->input('id_hakakses');

            $regUser       = User::create([
                                    'username'                              => $username,
                                    'password'                              => $password,
                                    'id_hakakses'                           => $id_hakakses,
                                    'nama'                                  => $nama
                                ]);
            
            if ($regUser) {
                return response()->json([
                    'success'           => true,
                    'message'           => 'Akun berhasil dibuat',
                    'registerGuruData'  => $regUser
                ], 200);

            } else {
                return response()->json([
                    'success'       => false,
                    'message'       => 'Akun gagal dibuat'
                ], 400);
            }
        }
    }
<?php
    namespace App\Http\Controllers\Api;

    use App\Http\Controllers\Controller;
    use Illuminate\Http\Request;
    use Illuminate\Support\Facades\DB;
    use App\Models\Komentar;

    class ApiMateri extends Controller
    {
        public function daftarMateri()
        {
            $materis    = DB::table('Materi')
                        ->leftJoin('KontenMateri', 'Materi.id_materi', '=', 'KontenMateri.id_materi')
                        ->leftJoin('User', 'Materi.id_user', '=', 'User.id_user')
                        ->orderBy('Materi.id_materi', 'DESC')    
                        ->select('Materi.id_materi', 'Materi.id_user', 'Materi.nama_materi', 'Materi.deskripsi_materi', 'Materi.durasi_materi', 'KontenMateri.id_kontenmateri', 'KontenMateri.kontenmateri', 'KontenMateri.file', 'User.id_hakakses', 'User.nama', 'User.username')
                        ->get();


            if ($materis !== null && count($materis) != 0) {
                return response()->json([
                    'success'                   => true,
                    'message'                   => 'Data berhasil ditampilkan',
                    'materi'                    => $materis
                ], 200);
            } else {
                return response()->json([
                    'success'                   => false,
                    'message'                   => 'Tidak ada hasil yang ditampilkan',
                    'materi'                    => $materis
                ], 200);
            }
            return response()->json([
                'success'                       => false,
                'message'                       => 'Ada kesalahan dalam sistem'
            ], 500);
        }

        public function komentar(Request $req, $idMateri)
        {
            
            $komen                  = new Komentar;

            $komen->id_materi       = $idMateri;
            $komen->id_user         = $req->id_user;
            $komen->komentar        = $req->komentar;


            if ($komen->save()) {
                return response()->json([
                    'success'           => true,
                    'message'           => 'Berhasil berkomentar',
                    'komentar'          => $komen
                ], 200);

            } else {
                return response()->json([
                    'success'       => false,
                    'message'       => 'Komentar gagal'
                ], 400);
            }
        }
    }
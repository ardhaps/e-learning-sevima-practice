<?php

namespace App\Http\Controllers\Konten;

use App\Http\Requests;
use Illuminate\Http\Request;
use Auth;
use App\Http\Controllers\Controller;
use App\Models\Komentar;

class KontenMahasiswaCrudCtl extends Controller
{
	public function tambahKomentar(Request $req, $idMateri)
    {
        $komenBaru    = new Komentar;

        $komenBaru->id_materi       = $idMateri;
        $komenBaru->id_user         = Auth::user()->id_user;
        $komenBaru->komentar        = $req->komentar;

        $komenBaru->save();
        
		return back();
    }
}

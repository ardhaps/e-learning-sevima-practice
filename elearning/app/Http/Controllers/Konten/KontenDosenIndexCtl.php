<?php

namespace App\Http\Controllers\Konten;

use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\DB;
use App\Models\Komentar;

class KontenDosenIndexCtl extends Controller
{
	public function index($idMateri)
    {

        $materi    = DB::table('Materi')
                        ->leftJoin('KontenMateri', 'Materi.id_materi', '=', 'KontenMateri.id_materi')
                        ->leftJoin('User', 'Materi.id_user', '=', 'User.id_user')
                        ->where('Materi.id_materi', $idMateri)
                        ->first();
        
        $komentars  = Komentar::
                        where('id_materi', $idMateri)
                        ->get();
        

		return view('kontendosen.kontendosen', compact('materi', 'komentars'));
    }


}

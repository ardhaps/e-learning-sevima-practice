<?php

namespace App\Http\Controllers\Download;

use App\Http\Controllers\Controller;

class DownloadCtl extends Controller
{
    public function downloadFile($file)
    {
        $fileUrl        = public_path().\Storage::url($file);
        $headers = [
            'Content-Disposition' => 'attachment; inline',
         ];

        return response()->download($fileUrl, $file, $headers);
    }
}
<?php

namespace App\Http\Controllers\Posting;

use Auth;
use Illuminate\Support\Facades\DB;
use App\Http\Controllers\Controller;


class PostingIndexCtl extends Controller
{
	public function index()
    {   
        $materis    = DB::table('Materi')
                    ->leftJoin('KontenMateri', 'Materi.id_materi', '=', 'KontenMateri.id_materi')
                    ->leftJoin('User', 'Materi.id_user', '=', 'User.id_user')
                    ->where('Materi.id_user', '=', Auth::user()->id_user)
                    ->orderBy('Materi.id_materi', 'DESC')    
                    ->get();
            
        
		return view('posting.posting', compact('materis'));
    }


}

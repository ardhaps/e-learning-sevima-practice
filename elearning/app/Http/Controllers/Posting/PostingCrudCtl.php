<?php

namespace App\Http\Controllers\Posting;

use App\Http\Requests;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

use App\Http\Controllers\Controller;
use App\Models\Komentar;
use App\Models\KontenMateri;
use App\Models\Materi;

class PostingCrudCtl extends Controller
{
	public function tambahPosting(Request $req)
    {
        $materi                     = new Materi;
        $materi->nama_materi        = $req->namaMateri;
        $materi->deskripsi_materi   = $req->deskripsiMateri;
        $materi->durasi_materi      = $req->durasiMateri;
        $materi->id_user            = Auth::user()->id_user;

        if ($materi->save()) {
            $konten                 = new KontenMateri;
            $konten->id_materi      = $materi->id;
            $konten->kontenmateri   = $req->kontenMateri;
            
            if (!is_null($req->fileKonten)) {
                $kontenFile             = $req->file('fileKonten');
                $fileName               = $kontenFile->getClientOriginalName();

                $path                   = $kontenFile->storeAs('public', $fileName);

                // $kontenFile->move("lampiran/", $fileName);
                $konten->file           = $fileName;
            }

            $konten->save();
        }

		return back();
    }

    public function hapusPosting($idMateri)
    {
        if (count(Komentar::where('id_materi', $idMateri)->get()) > 0) 
        {
            $hapusKomen     = Komentar::
                                where('id_materi', $idMateri)
                                ->delete();
        }

        if (count(KontenMateri::where('id_materi', $idMateri)->get()) > 0) 
        {
            $hapusKonten    = KontenMateri::
                                where('id_materi', $idMateri)
                                ->delete();
        }
        
        $hapusMateri    = Materi::
                        where('id_materi', $idMateri)
                        ->delete();
        
		return back();
    }


}

@extends('applayouts.lay-register')
@section('content')
<div class="limiter">
    <div class="container-login100">
        <div class="wrap-login100">
            <form class="login100-form validate-form" action="{{ route('register') }}" method="POST">
                {{ csrf_field() }}
                <span class="login100-form-title p-b-26">
                    Register User E-Learning
                </span>

                <div class="wrap-input100 validate-input" data-validate="Masukkan nama">
                    <input class="input100" type="text" name="nama">
                    <span class="focus-input100" data-placeholder="Nama"></span>
                </div>

                <div class="form-group row validate-input" data-validate="Pilih jenis akses">
                    
                    <div class="col-lg-4">
                        <div class="custom-control custom-radio styled-radio">
                            <input class="custom-control-input" type="radio" name="hakakses" id="hakakses1" value="1" checked>
                            <label class="custom-control-descfeedback" for="hakakses1">Dosen</label>
                        </div>
                    </div>
                    <div class="col-lg-4">
                        <div class="custom-control custom-radio styled-radio">
                            <input class="custom-control-input" type="radio" name="hakakses" id="hakakses2" value="2">
                            <label class="custom-control-descfeedback" for="hakakses2">Mahasiswa</label>
                        </div>
                    </div>
                </div>

               
                <div class="wrap-input100 validate-input" data-validate="Masukkan username">
                    <input class="input100" type="text" name="username">
                    <span class="focus-input100" data-placeholder="Username"></span>
                </div>

                <div class="wrap-input100 validate-input" data-validate="Enter password">
                    <span class="btn-show-pass">
                        <i class="zmdi zmdi-eye"></i>
                    </span>
                    <input class="input100" type="password" name="password">
                    <span class="focus-input100" data-placeholder="Password"></span>
                </div>

                <div class="container-login100-form-btn">
                    <div class="wrap-login100-form-btn">
                        <div class="login100-form-bgbtn"></div>
                        <button type="submit" class="login100-form-btn">
                            Register
                        </button>
                    </div>
                </div>

                <div class="text-center p-t-115">
                    <span class="txt1">
                        Sudah punya akun?
                    </span>

                    <a class="txt2" href="{{ route('showLogin') }}">
                        Login ke sini
                    </a>
                </div>
            </form>
        </div>
    </div>
</div>
<div id="dropDownSelect1"></div>
@endsection

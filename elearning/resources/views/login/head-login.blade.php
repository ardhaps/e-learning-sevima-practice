{!! Html::style(asset('css/iconic/material-design-iconic-font.min.css')) !!}
{!! Html::style(asset('css/hamburgers/hamburgers.min.css')) !!}
{!! Html::style(asset('css/animsition/animsition.min.css')) !!}
{!! Html::style(asset('css/select2/select2.min.css')) !!}
{!! Html::style(asset('css/daterangepicker/daterangepicker.css')) !!}
{!! Html::style(asset('css/util.css')) !!}
{!! Html::style(asset('css/main.css')) !!}
</head>

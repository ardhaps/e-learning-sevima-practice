<!DOCTYPE html>
<html lang="en">
  <head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <title>Latihan Pemrograman Web E-Learning</title>
    <meta name="description" content="Latihan Pemrograman Web E-Learning">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <meta name="csrf-token" content="{{ csrf_token() }}">
    
    {!! Html::script('js/webfont.js') !!}
    <script>
      WebFont.load({
        google: {"families":["Montserrat:400,500,600,700","Noto+Sans:400,700"]},
        active: function() {
            sessionStorage.fonts = true;
        }
      });
    </script>
    <link rel="shortcut icon" type="image/x-icon" href="img/favicon.ico">
    <link rel="icon" type="image/x-icon" href="img/favicon.ico">
    <link rel="icon" type="image/png" href="img/favicon.ico"/>

	{!! Html::style(asset('css/bootstrap/bootstrap.min.css')) !!}
  {!! Html::style(asset('css/elisyam/elisyam-1.5.min.css')) !!}
  {!! Html::style(asset('css/font-awesome/font-awesome.min.css')) !!}

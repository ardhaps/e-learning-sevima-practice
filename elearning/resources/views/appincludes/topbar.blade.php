<body id="page-top">
<!-- Begin Preloader -->
<div id="preloader">
    <div class="canvas">
        {{ Html::image('img/avatar.jpg', 'logo', array('class' => 'loader-logo')) }}

        <div class="spinner"></div>   
    </div>
</div>
<!-- End Preloader -->
<div class="page db-social">
    <!-- Begin Header -->
    <header class="header">
        <nav class="navbar fixed-top">
            <!-- Begin Topbar -->
            <div class="navbar-holder d-flex align-items-center align-middle justify-content-between">
                <!-- Begin Logo -->
                <div class="navbar-header" style="text-align:center; font-size: 24px;">
                    SISTEM INFORMASI E-LEARNING
                </div>
                <!-- End Logo -->
                <!-- Begin Navbar Menu -->
                <ul class="nav-menu list-unstyled d-flex flex-md-row align-items-md-center pull-right">
                    <!-- User -->
                    <li class="nav-item dropdown">
                        <a id="user" rel="nofollow" data-target="#" href="#" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false" class="nav-link">
                            {{ Html::image('img/avatar.jpg', '...', array('class' => 'avatar rounded-circle')) }}
                        </a>
                        <ul aria-labelledby="user" class="user-size dropdown-menu">
                            <li class="welcome">
                                    {{ Html::image('img/avatar.jpg', '...', array('class' => 'ti-power-off')) }}
                            </li>
                            <li>
                                <a class="dropdown-item no-padding-bottom" style="text-align:center"> 
                                    Selamat datang, <u>{{Auth::user()->nama}}</u>
                                </a>
                            </li>
                            <li>
                                <a class="dropdown-item no-padding-bottom" style="text-align:center"> 
                                    Akses : <u>{{Auth::user()->hakAkse->hakakses}}</u>
                                </a>
                            </li>
                            <hr />
                            <li> 
                                <a href="{{ route('logout') }}" class="dropdown-item no-padding-bottom"> 
                                    LOGOUT
                                </a>
                            </li>
                            <br />
                        </ul>
                    </li>
                    <!-- End User -->
                </ul>
                <!-- End Navbar Menu -->
            </div>
            <!-- End Topbar -->
        </nav>
    </header>
    <!-- End Header -->

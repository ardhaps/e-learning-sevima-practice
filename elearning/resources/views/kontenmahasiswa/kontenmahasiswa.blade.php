@extends('applayouts.lay-kontenmahasiswa')
@section('content')
        <!-- Begin Page Content -->
        <div class="page-content d-flex">
            
            <!-- Begin Content -->
            <div class="content-inner compact" style="margin-left:0px;width:calc(100%);">
                <div class="container-fluid newsfeed">
                    <div class="row justify-content-center">
                        <div class="col-xl-9">
                            <div class="row">
                                <!-- Begin Timeline -->
                                <div class="col-xl-12">




                                <form class="form-horizontal" method="POST">
                                {{ csrf_field() }}
                                    @if(count($materi) > 0)
                                        <!-- Begin DAFTAR KONTEN -->
                                        <div class="widget has-shadow">
                                            <!-- Begin Widget Header -->
                                            <div class="widget-header d-flex align-items-center">
                                                <div class="user-image">
                                                    {{ Html::image('img/avatar.jpg', '...', array('class' => 'avatar rounded-circle')) }}
                                                </div>
                                                <div class="d-flex flex-column mr-auto">
                                                    <div class="title">
                                                        <span class="username">{{ $materi->nama }}</span>
                                                    </div>
                                                    <div class="time">52 min ago</div>
                                                </div>
                                               
                                            </div>
                                            <!-- End Widget Header -->
                                            <!-- Begin Widget Body -->
                                            <div class="widget-body">
                                                <p>
                                                    {{ $materi->kontenmateri }}
                                                </p>
                                                <div class="row justify-content-center">
                                                    <div class="col-xl-11">
                                                        <div class="row post-video">
                                                            <div class="col-xl-5 col-lg-5 col-sm-5 col-12">
                                                                <div class="hover-img">
                                                                    <li class="welcome">
                                                                            {{ Html::image('img/avatar.jpg', '...', array('class' => 'ti-power-off')) }}
                                                                    </li>
                                                                </div>
                                                            </div>
                                                            <div class="col-xl-7 col-lg-7 col-sm-7 col-12 mt-auto mb-auto">
                                                                <h3>{{ $materi->nama_materi }}</h3>
                                                                <p>{{ $materi->durasi_materi }} jam</p>
                                                                <p>{{ $materi->deskripsi_materi }}</p>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                                <br />
                                                @if(!is_null($materi->file) && $materi->file != '')
                                                <div>Lampiran : (Klik file di bawah untuk download)</div>
                                                <div class="attachments">
                                                    <div class="file-attachment">
                                                        <div class="item">
                                                            <a href="{{ route('downloadFileMahasiswa', $materi->file) }}">
                                                                <i class="la la-paperclip"></i>
                                                                {{ $materi->file }}
                                                            </a>
                                                        </div>
                                                    </div>
                                                </div>
                                                @endif
                                            </div>
                                            <!-- End Widget Body -->
                                            <!-- Begin Comments -->

                                            @if(count($komentars) > 0)
                                                @foreach ($komentars as $komentar)
                                                <div class="comments">
                                                        <div class="comments-header d-flex align-items-center">
                                                            <div class="user-image">
                                                                {{ Html::image('img/avatar.jpg', '...', array('class' => 'avatar rounded-circle')) }}
                                                                
                                                            </div>
                                                            <div class="d-flex flex-column mr-auto">
                                                                <div class="title">
                                                                    <span class="username">{{ $komentar->user->nama }}</span>
                                                                </div>
                                                                <div class="time">10 min ago</div>
                                                            </div>
                                                        </div>
                                                        
                                                        <div class="comments-body">
                                                            <p>
                                                                 {{ $komentar->komentar }}
                                                            </p>
                                                        </div>
                                                        
                                                    </div>
                                                    <!-- End Comments -->
                                                @endforeach
                                            @endif
                                                    
                                                    <!-- Begin Publisher -->
                                                    <div class="publisher publisher-multi">
                                                        <textarea class="publisher-input" rows="1" type="text" id="komentar" name="komentar">
                                                        </textarea>
                                                        <div class="publisher-bottom d-flex justify-content-end">
                                                            <button class="btn btn-gradient-01" type="submit">Komentar</button>
                                                        </div>
                                                    </div>
                                                    <!-- End Publisher -->
                                                </div>
                                    <!-- End DAFTAR KONTEN -->
                                    @endif
                                </form>
@endsection

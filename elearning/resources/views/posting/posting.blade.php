@extends('applayouts.lay-posting')
@section('content')
        <!-- Begin Page Content -->
        <div class="page-content d-flex">
            
            <!-- Begin Content -->
            <div class="content-inner compact" style="margin-left:0px;width:calc(100%);">
                <div class="container-fluid newsfeed">
                    <div class="row justify-content-center">
                        <div class="col-xl-9">
                            <div class="row">
                                
                                <!-- Begin Timeline -->
                                <div class="col-xl-12">
                                    <div class="widget has-shadow">
                                        <!-- Begin Publisher -->
                                        <form action="{{ route('tambahPosting') }}" class="form-horizontal" method="POST" enctype="multipart/form-data">
                                        {{ csrf_field() }}
                                            <div class="publisher publisher-multi bg-white">
                                                    <div class="widget-header bordered no-actions d-flex align-items-center">
                                                        <h4>POSTING MATERI</h4>
                                                    </div>
                                                    <br />
                                                {{-- <textarea class="publisher-input" rows="4" placeholder="Posting di sini.."></textarea> --}}
                                                <div class="form-group row d-flex align-items-center mb-5">
                                                    <label class="col-lg-2 form-control-label d-flex ">Materi</label>
                                                    <div class="col-lg-10">
                                                        <input type="text" id="namaMateri" name="namaMateri" class="form-control" placeholder="Masukkan nama materi">
                                                    </div>
                                                </div>
                                                <div class="form-group row d-flex align-items-center mb-5">
                                                    <label class="col-lg-2 form-control-label d-flex ">Deskripsi</label>
                                                    <div class="col-lg-10">
                                                        <input type="text" id="deskripsiMateri" name="deskripsiMateri" class="form-control" placeholder="Masukkan deskripsi materi">
                                                    </div>
                                                </div>
                                                <div class="form-group row d-flex align-items-center mb-5">
                                                    <label class="col-lg-2 form-control-label d-flex ">Durasi</label>
                                                    <div class="col-lg-10">
                                                        <input type="number" id="durasiMateri" name="durasiMateri" class="form-control" placeholder="Lama durasi materi (angka desimal)">
                                                    </div>
                                                </div>
                                                <div class="form-group row d-flex align-items-center mb-5">
                                                    <label class="col-lg-2 form-control-label d-flex ">Konten Materi</label>
                                                    <div class="col-lg-10">
                                                            <textarea class="form-control" id="kontenMateri" name="kontenMateri" placeholder="Tulis konten materi di sini ..."></textarea>
                                                        </div>
                                                </div>

                                                <div class="publisher-bottom">
                                                    <div class="d-inline-block">
                                                        Pilih file :
                                                        <div class=" d-inline-block">
                                                            {{-- <label for="fileKonten">
                                                                <a class="publisher-btn"><i class="la la-paperclip"></i></a>                                                   
                                                            </label> --}}
                                                            <input id="fileKonten" name="fileKonten" accept="image/x-png,image/gif,image/jpeg,video/mp4,video/x-m4v,video/*,application/pdf,.doc,.docx,application/msword,application/vnd.openxmlformats-officedocument.wordprocessingml.document" type="file"/>
                                                        </div>
                                                    </div>
                                                    
                                                    <div class="d-inline-block pull-right">
                                                        <button type="submit" class="btn btn-gradient-01">Posting</button>
                                                    </div>
                                                </div>
                                            </div>
                                        </form>
                                        <!-- End Publisher -->
                                    </div>




                                    @if(count($materis) > 0)
                                        @foreach ($materis as $materi)
                                        <!-- Begin DAFTAR KONTEN -->
                                        <div class="widget has-shadow">
                                            <!-- Begin Widget Header -->
                                            <div class="widget-header d-flex align-items-center">
                                                <div class="user-image">
                                                    {{ Html::image('img/avatar.jpg', '...', array('class' => 'avatar rounded-circle')) }}                                            
                                                </div>
                                                <div class="d-flex flex-column mr-auto">
                                                    <div class="title">
                                                        <span class="username">{{Auth::user()->nama}}</span>
                                                    </div>
                                                    <div class="time">52 min ago</div>
                                                </div>
                                                <div class="widget-options">
                                                    <div class="dropdown">
                                                        <button type="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false" class="dropdown-toggle">
                                                            <i class="la la-angle-down"></i>
                                                        </button>
                                                        <div class="dropdown-menu">
                                                            <form action="{{ route('hapusPosting', $materi->id_materi) }}" class="form-horizontal" method="POST">
                                                            {{ csrf_field() }}
                                                                <button type="submit" class="dropdown-item"> 
                                                                    <i class="la la-trash"></i>Hapus Posting
                                                                </button>
                                                            </form>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                            <!-- End Widget Header -->
                                            <!-- Begin Widget Body -->
                                            <div class="widget-body">
                                                <p>
                                                    {{ str_limit($materi->kontenmateri, 100, " . . . . .") }}
                                                </p>
                                                <div class="row justify-content-center">
                                                    <div class="col-xl-11">
                                                        <div class="row post-video">
                                                            <div class="col-xl-5 col-lg-5 col-sm-5 col-12">
                                                                <div class="hover-img">
                                                                    <li class="welcome">
                                                                            {{ Html::image('img/avatar.jpg', '...', array('class' => 'ti-power-off')) }}
                                                                    </li>
                                                                </div>
                                                            </div>
                                                            <div class="col-xl-7 col-lg-7 col-sm-7 col-12 mt-auto mb-auto">
                                                                <h3>{{ $materi->nama_materi }}</h3>
                                                                <p>{{ $materi->durasi_materi }} jam</p>
                                                                <p>{{ $materi->deskripsi_materi }}</p>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                            <!-- End Widget Body -->
                                        <!-- Begin Publisher -->
                                        <div class="publisher publisher-multi">
                                            @if(!is_null($materi->file) && $materi->file != '')
                                            <div>Lampiran : (Klik file di bawah untuk download)</div>
                                            <div class="attachments">
                                                <div class="file-attachment">
                                                    <div class="item">
                                                        <a href="{{ route('downloadFileDosen', $materi->file) }}">
                                                            <i class="la la-paperclip"></i>
                                                            {{ $materi->file }}
                                                        </a>
                                                    </div>
                                                </div>
                                            </div>
                                            @endif

                                            <div class="publisher-bottom d-flex justify-content-end" style="text-align:center;">
                                                <button class="btn btn-gradient-01" style="margin:0 auto;display:block;">
                                                    <a href="{{ url('kontendosen') }}/{{ $materi->id_materi }}">
                                                        Detail Konten
                                                    </a>
                                                </button>
                                            </div>
                                        </div>
                                        <!-- End Publisher -->
                                    </div>
                                    <!-- End DAFTAR KONTEN -->
                                    @endforeach
                                @endif
@endsection
